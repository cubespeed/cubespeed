/*
 *   ValaRef, data analysis and visualisation utility
 *   Copyright (C) 2010, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of ValaRef.
 *
 *   ValaRef is free software: you can redistribute it and/or modify it
 *   under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   ValaRef is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with ValaRef. If not, see <http://www.gnu.org/licenses/>.
 */

using CubeSpeed, GLib;

int
main(string[] args)
{
	var ui = new GtkUi();

	ui.exec(ref args);

	return 0;
}

/*
 *   CubeSpeed, data analysis and visualisation utility
 *   Copyright (C) 2011, Mariusz Adamski <mariusz.adamski@gmail.com>
 *
 *   This file is part of CubeSpeed.
 *
 *   ValaPlot is free software: you can redistribute it and/or modify it
 *   under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   ValaPlot is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with ValaPlot. If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

public class CubeSpeed.GtkUi : GLib.Object
{
	public GtkUi() {}

	public bool exec(ref unowned string[] args) {
		Gtk.init(ref args);
		var window = new GtkWindow();
		window.show();
		Gtk.main();
		return true;
	}
}

public class CubeSpeed.GtkWindow : Window
{
	public Label time;
	private uint timertag;
	private bool started = false;
	private Timer timer;

	public GtkWindow() {
		set_title("CubeSpeed");
		var vbox = new VBox(true, 1);
		vbox.set_homogeneous(false);
		var tbox = new HButtonBox();
		tbox.set_layout(ButtonBoxStyle.CENTER);
		tbox.set_spacing(5);
		add(vbox);

		string str = "00'%05.2f\"";
		str = str.printf(0.0);
		time  = new Label(str);
		Pango.AttrList attrs = new Pango.AttrList();
		attrs.insert(Pango.attr_scale_new(7));
		attrs.insert(Pango.attr_family_new("courier"));
		time.set_attributes(attrs);
		var start = new Button.with_label("Start");
		start.set_image(new Image.from_stock(STOCK_MEDIA_PLAY,
		                IconSize.BUTTON));
		var stop = new  Button.with_label("Stop");
		stop.set_image(new Image.from_stock(STOCK_MEDIA_STOP,
		               IconSize.BUTTON));
		var quit  = new Button.from_stock(STOCK_QUIT);
		var info = new Label(
		    "Hint: You can press <span " +
		    "foreground=\"blue\">[space]</span> to start or stop the timer");
		info.set_use_markup(true);
		tbox.pack_start(start, false, false, 0);
		tbox.pack_start(stop, false, false, 0);
		tbox.pack_start(quit, false, false, 0);
		vbox.pack_start(time, true, true, 0);
		vbox.pack_start(tbox, false, false, 10);
		vbox.pack_start(info, false, false, 10);

		this.destroy.connect(main_quit);
		this.key_press_event.connect(key_pressed);
		start.clicked.connect(start_timer);
		stop.clicked.connect(stop_timer);
		quit.clicked.connect(main_quit);

		show_all();

		timer = new Timer();
		timer.stop();
	}

	public bool key_pressed(Gdk.EventKey event) {
		if (event.keyval != 32)
			return false; /* Return false to propagate event
			               * further */

		if (started)
			stop_timer();
		else
			start_timer();
		return true;          /* Event handled; don't propagate */
	}

	public void start_timer() {
		if (started)
			return;
		timer.reset();
		timer.start();
		timertag = Timeout.add(10, update);
		started = true;
	}

	public void stop_timer() {
		if (!started)
			return;
		timer.stop();
		Source.remove(timertag);
		started = false;
	}

	public bool update() {
		double t = timer.elapsed();
		int seconds = (int) t;
		int minutes = seconds / 60;
		double frac = t - minutes * 60;;

		string str = "%02d'%05.2f\"";
		str = str.printf(minutes, frac);
		time.set_label(str);
		return true;
	}
}
